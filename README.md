# Ansible Role: PHP Tideways
Installs the Tideways PHP Profile Extension on CentOS servers.

## Requirements

This role require Ansible 2.9 or higher.

This role was designed for:
  - RHEL/CentOS 7/8
  
## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    tideways_api_key: ""

The API Key is required configuration to make Tideways work.

    tideways_sample_rate: 25
    
The percentage of how many requests are executed with the Timeline Profiler activated ("being sampled"). All other requests are still monitored for their performance and potential errors/exceptions. 
   
    tideways_enable_cli: false
    
By default the sample rate does not affect CLI scripts, because they are often running as very long running cronjobs and perform non-transactional high intensity tasks. If you want to enable tracing in the CLI based on the sample rate, then set this variable to TRUE.
   
    tideways_service: ''
    
The services feature allows you to group transactions into different sets that belong together and get aggregated together in their own chart and overall response time.
    
    tideways_dynamic_tracepoints_enable: true
    
With tracepoints you can increase the amount of traces collected from selected endpoints and even elevate them to include callgraph information. They can be activated directly from the user interface by clicking "Boost Traces" buttons.
    
    tideways_features_deprecations: true
    
Deprecations Tracking is similar to Exception Tracking and logs all PHP errors with the E_DEPRECATED and E_USER_DEPRECATED error type. Deprecations are aggregated based on the location they occur at so that you can easily spot code that needs to be adapted for futures upgrades.
    
    tideways_environment: 'production'
    
Environments allow you to collect performance data and traces from different "environments" of your project. For example, data is kept separate to avoid interference of testing data with production data. Comparing them is supported to find performance regressions before they get into production.

## Dependencies

- geerlingguy.php

## License

MIT / BSD

## Author Information

This role was created in 2020 by [Maksim Soldatjonok](https://www.maksold.com/).